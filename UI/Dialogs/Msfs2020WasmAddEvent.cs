﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MobiFlight.UI.Dialogs
{
    public partial class Msfs2020WasmAddEvent : Form
    {
        public Msfs2020WasmAddEvent()
        {
            InitializeComponent();
        }

        private void AddEventPreview_Click(object sender, EventArgs e)
        {
            AddEventPreviewCode.Text = AddEventName.Text + "#" + AddEventVariable.Text;
        }

        private void AddEventClearForms_Click(object sender, EventArgs e)
        {
            AddEventName.Clear();
            AddEventPreviewCode.Clear();
            AddEventVariable.Clear();
        }

        private void AddEventName_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = (e.KeyChar == (char)Keys.Space);
        }

    }
}