﻿
namespace MobiFlight.UI.Dialogs
{
    partial class Msfs2020WasmAddEvent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Msfs2020WasmAddEvent));
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.AddEventPreview = new System.Windows.Forms.Button();
            this.AddEventSaveAndAddNewEvent = new System.Windows.Forms.Button();
            this.AddEventSaveAndClose = new System.Windows.Forms.Button();
            this.AddEventClearForms = new System.Windows.Forms.Button();
            this.AddEventPreviewCode = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.AddEventVariable = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.AddEventName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBox1.Location = new System.Drawing.Point(12, 12);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(776, 29);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "Please use this section to add a new event or variable to your personal events fi" +
    "le. The modifications made here have no effect on the default configuration and " +
    "events provided by Mobiflight.";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.AddEventPreview);
            this.panel1.Controls.Add(this.AddEventSaveAndAddNewEvent);
            this.panel1.Controls.Add(this.AddEventSaveAndClose);
            this.panel1.Controls.Add(this.AddEventClearForms);
            this.panel1.Controls.Add(this.AddEventPreviewCode);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.AddEventVariable);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.AddEventName);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 47);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(776, 391);
            this.panel1.TabIndex = 1;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(3, 17);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(770, 21);
            this.comboBox1.TabIndex = 48;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 13);
            this.label2.TabIndex = 47;
            this.label2.Text = "Existing Function Name";
            // 
            // AddEventPreview
            // 
            this.AddEventPreview.Location = new System.Drawing.Point(158, 342);
            this.AddEventPreview.Name = "AddEventPreview";
            this.AddEventPreview.Size = new System.Drawing.Size(143, 23);
            this.AddEventPreview.TabIndex = 46;
            this.AddEventPreview.Text = "Preview new function";
            this.AddEventPreview.UseVisualStyleBackColor = true;
            // 
            // AddEventSaveAndAddNewEvent
            // 
            this.AddEventSaveAndAddNewEvent.Location = new System.Drawing.Point(481, 342);
            this.AddEventSaveAndAddNewEvent.Name = "AddEventSaveAndAddNewEvent";
            this.AddEventSaveAndAddNewEvent.Size = new System.Drawing.Size(143, 23);
            this.AddEventSaveAndAddNewEvent.TabIndex = 45;
            this.AddEventSaveAndAddNewEvent.Text = "Save and add new event";
            this.AddEventSaveAndAddNewEvent.UseVisualStyleBackColor = true;
            // 
            // AddEventSaveAndClose
            // 
            this.AddEventSaveAndClose.Location = new System.Drawing.Point(630, 342);
            this.AddEventSaveAndClose.Name = "AddEventSaveAndClose";
            this.AddEventSaveAndClose.Size = new System.Drawing.Size(143, 23);
            this.AddEventSaveAndClose.TabIndex = 44;
            this.AddEventSaveAndClose.Text = "Save and close";
            this.AddEventSaveAndClose.UseVisualStyleBackColor = true;
            // 
            // AddEventClearForms
            // 
            this.AddEventClearForms.Location = new System.Drawing.Point(3, 342);
            this.AddEventClearForms.Name = "AddEventClearForms";
            this.AddEventClearForms.Size = new System.Drawing.Size(143, 23);
            this.AddEventClearForms.TabIndex = 43;
            this.AddEventClearForms.Text = "Clear forms";
            this.AddEventClearForms.UseVisualStyleBackColor = true;
            // 
            // AddEventPreviewCode
            // 
            this.AddEventPreviewCode.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.AddEventPreviewCode.Cursor = System.Windows.Forms.Cursors.Default;
            this.AddEventPreviewCode.Location = new System.Drawing.Point(3, 216);
            this.AddEventPreviewCode.Multiline = true;
            this.AddEventPreviewCode.Name = "AddEventPreviewCode";
            this.AddEventPreviewCode.ReadOnly = true;
            this.AddEventPreviewCode.Size = new System.Drawing.Size(770, 100);
            this.AddEventPreviewCode.TabIndex = 39;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(0, 200);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(152, 13);
            this.label4.TabIndex = 42;
            this.label4.Text = "Preview of new event/variable";
            // 
            // AddEventVariable
            // 
            this.AddEventVariable.AcceptsTab = true;
            this.AddEventVariable.Location = new System.Drawing.Point(3, 97);
            this.AddEventVariable.Multiline = true;
            this.AddEventVariable.Name = "AddEventVariable";
            this.AddEventVariable.Size = new System.Drawing.Size(770, 100);
            this.AddEventVariable.TabIndex = 41;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(0, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(214, 13);
            this.label3.TabIndex = 40;
            this.label3.Text = "Event, variable or custom code (H:, K: or L:)";
            // 
            // AddEventName
            // 
            this.AddEventName.AcceptsTab = true;
            this.AddEventName.Location = new System.Drawing.Point(3, 58);
            this.AddEventName.Name = "AddEventName";
            this.AddEventName.Size = new System.Drawing.Size(770, 20);
            this.AddEventName.TabIndex = 38;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 37;
            this.label1.Text = "Function Name";
            // 
            // Msfs2020WasmAddEvent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.textBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Msfs2020WasmAddEvent";
            this.Text = "MSFS 2020 - Add new event/variable";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button AddEventPreview;
        private System.Windows.Forms.Button AddEventSaveAndAddNewEvent;
        private System.Windows.Forms.Button AddEventSaveAndClose;
        private System.Windows.Forms.Button AddEventClearForms;
        private System.Windows.Forms.TextBox AddEventPreviewCode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox AddEventVariable;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox AddEventName;
        private System.Windows.Forms.Label label1;
    }
}